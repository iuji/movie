package br.com.iuji.movies.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.iuji.movies.R
import br.com.iuji.navigation.features.login.LoginNavigation
import org.koin.android.ext.android.inject

class SplashScreenActivity : AppCompatActivity(R.layout.activity_splash_screen) {

    private val loginNavigation: LoginNavigation by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginNavigation.startActivity(this)
    }
}