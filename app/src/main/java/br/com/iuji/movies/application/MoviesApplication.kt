package br.com.iuji.movies.application

import android.app.Application
import org.koin.core.context.startKoin

open class MoviesApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    open fun setupKoin() {
        startKoin(appDeclaration = KoinDeclarationProvider.get(this))
    }
}