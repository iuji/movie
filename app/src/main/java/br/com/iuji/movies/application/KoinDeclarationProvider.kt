package br.com.iuji.movies.application

import android.app.Application
import br.com.iuji.features.home.di.HomeModule
import br.com.iuji.features.login.di.LoginModule
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.KoinAppDeclaration

object KoinDeclarationProvider {

    fun get(application: Application): KoinAppDeclaration = {
        androidContext(application)

        LoginModule().load()
        HomeModule().load()
    }
}