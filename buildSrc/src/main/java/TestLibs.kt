object TestLibs {
    // Espresso
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    const val espressoIntents = "androidx.test.espresso:espresso-intents:${Versions.espressoVersion}"
    const val espressoContrib = "androidx.test.espresso:espresso-contrib:${Versions.espressoVersion}"
    const val espressoIdling = "androidx.test.espresso:espresso-idling-resource:${Versions.espressoVersion}"
    const val junitExt = "androidx.test.ext:junit:${Versions.testExtJunit}"
    const val koin = "io.insert-koin:koin-test:${Versions.koinVersion}"
    const val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebserverVersion}"

    const val arcCore = "androidx.arch.core:core-testing:${Versions.androidCoreVersion}"
    const val junit = "junit:junit:${Versions.junitTest}"
    const val kotlinTest = "org.jetbrains.kotlin:kotlin-test:${Versions.kotlinVersion}"
    const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesVersion}"
    const val core = "androidx.test:core:${Versions.coreTest}"
    const val turbine = "app.cash.turbine:turbine:${Versions.turbine}"
    const val mockk = "io.mockk:mockk:${Versions.mockk}"
    const val mockkAndroid = "io.mockk:mockk-android:${Versions.mockk}"
    const val okhttp3Idling = "com.jakewharton.espresso:okhttp3-idling-resource:${Versions.okhttp3IdlingVersion}"

}
