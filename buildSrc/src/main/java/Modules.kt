object Modules {
    const val navigation = ":navigation"
    const val core = ":core"
    const val login = ":features:login"
    const val home = ":features:home"
}