object Versions {
    const val kotlinVersion = "1.5.21"
    const val kotlinxSerialization = "1.2.1"

    // Google Libs
    const val constraintLayout = "2.0.1"
    const val androidMaterial = "1.3.0"
    const val coroutinesVersion = "1.5.1"
    const val appcompat = "1.2.0"
    const val lifecycle = "2.3.1"
    const val coreKtx = "1.3.1"
    const val swipeRefreshLayoutVersion = "1.1.0"

    // External Libs
    const val picassoVersion = "2.71828"
    const val retrofitVersion = "2.6.4"
    const val retrofitKSerializationConverterVersion = "0.8.0"
    const val okHttpVersion = "4.9.0"
    const val koinVersion = "2.2.3"
    const val okhttp3IdlingVersion = "1.0.0"

    // Test Libs
    const val espressoVersion = "3.3.0"
    const val mockWebserverVersion = "4.6.0"
    const val androidCoreVersion = "2.1.0"
    const val junitTest = "4.13"
    const val testExtJunit = "1.1.2"
    const val fragment = "1.3.1"
    const val coreTest = "1.3.0"
    const val turbine = "0.5.2"
    const val mockk = "1.11.0"
}
