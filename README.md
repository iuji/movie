# Projeto filmes
Esse projeto consiste em um aplicativo de listagem de filmes que faz requisições em uma API.

# Descrição do projeto
É multi-modular e segue práticas de arquitetura limpa e princípios do SOLID.
Entre as bibliotecas utilizadas, constam:
- Retrofit (para fazer chamadas REST na API)
- Coroutines/Flow (para programação reativa/concorrente)
- Kotlin Serialization (para serialização e desserialização)
- View Binding
- Koin (para injeção de dependência)
- junit (para testes unitários)
- Espresso (para testes de interface)
- mockk (para mocks necessários em testes)
- mockWebServer (para mocks de API), etc.