package br.com.iuji.navigation.features.home

import android.content.Context

interface HomeNavigation {
    fun startActivity(context: Context)
}