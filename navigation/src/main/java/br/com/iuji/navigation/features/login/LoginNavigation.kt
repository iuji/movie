package br.com.iuji.navigation.features.login

import android.content.Context

interface LoginNavigation {
    fun startActivity(context: Context)
}