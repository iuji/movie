package br.com.iuji.core.mapper

interface Mapper<S, T> {
    fun map(source: S): T
}
