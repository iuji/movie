package br.com.iuji.features.login.navigation

import android.content.Context
import android.content.Intent
import br.com.iuji.features.login.presentation.LoginActivity
import br.com.iuji.navigation.features.login.LoginNavigation

class LoginNavigationImpl : LoginNavigation {
    override fun startActivity(context: Context) {
        val intent = Intent(context, LoginActivity::class.java)
        context.startActivity(intent)
    }
}