package br.com.iuji.features.login.di

import br.com.iuji.features.login.navigation.LoginNavigationImpl
import br.com.iuji.navigation.features.login.LoginNavigation
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

class LoginModule {

    private val navigation = module {
        factory<LoginNavigation> {
            LoginNavigationImpl()
        }
    }

    fun load() {
        loadKoinModules(navigation)
    }
}