package br.com.iuji.features.login.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.iuji.features.login.databinding.ActivityLoginBinding
import br.com.iuji.navigation.features.home.HomeNavigation
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val homeNavigation: HomeNavigation by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupListeners()
    }

    private fun setupListeners() {
        binding.loginButton.setOnClickListener {
            homeNavigation.startActivity(this)
        }
    }
}