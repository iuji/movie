package br.com.iuji.features.home.presentation

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import br.com.iuji.features.home.R
import br.com.iuji.features.home.rules.OkHttpIdlingResourceRule
import br.com.iuji.features.home.setup.FileReader
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.koin.test.inject
import java.net.HttpURLConnection

private const val SUCCESS_RESPONSE_PATH = "home/home_movies_success_response.json"
private const val ERROR_RESPONSE_PATH = "home/api_generic_error_response.json"

@RunWith(AndroidJUnit4::class)
class HomeActivityTest : KoinTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(HomeActivity::class.java)

    @get:Rule
    var rule = OkHttpIdlingResourceRule()

    private val robot: HomeActivityRobot by inject()

    private val mockWebServer = MockWebServer()

    @Before
    fun setup() {
        setupMockWebServer()
    }

    @After
    fun tearDown() {
        tearDownMockWebServer()
    }

    @Test
    fun whenActivityIsStarted_shouldDisplayRecyclerView() {
        mockWebServer.enqueue(getSuccessResponse())

        robot.apply {
            checkVisibility(R.id.moviesRecycler)
        }
    }

    @Test
    fun whenActivityIsStarted_shouldDisplayRecyclerViewItemsCorrectly() {
        mockWebServer.enqueue(getSuccessResponse())

        robot.apply {
            checkRecyclerViewItemText(R.id.moviesRecycler, 15, "Venom: Tempo de Carnificina")
        }
    }

    @Test
    fun whenActivityIsStarted_shouldDisplayError() {
        mockWebServer.enqueue(getErrorResponse())

        robot.apply {
            checkVisibility(R.id.errorView)
            textShouldBeCorrect(R.id.homeGenericErrorTitle, "Deu ruim!")
            textShouldBeCorrect(
                R.id.homeGenericErrorMsg,
                "Tivemos uma falha no sistema e não conseguimos carregar as informações"
            )
            textShouldBeCorrect(R.id.homeGenericErrorButton, "Tentar novamente")
        }
    }

    private fun getSuccessResponse(): MockResponse {
        return setHttpCode(HttpURLConnection.HTTP_OK).setBody(
            FileReader.readStringFromFile(
                SUCCESS_RESPONSE_PATH
            )
        )
    }

    private fun getErrorResponse(): MockResponse {
        return setHttpCode(HttpURLConnection.HTTP_BAD_REQUEST).setBody(
            FileReader.readStringFromFile(
                ERROR_RESPONSE_PATH
            )
        )
    }

    private fun setHttpCode(statusCode: Int) = MockResponse().setResponseCode(statusCode)

    private fun setupMockWebServer() {
        mockWebServer.start(8080)
    }

    private fun tearDownMockWebServer() {
        mockWebServer.shutdown()
    }
}