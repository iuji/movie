package br.com.iuji.features.home.setup

import android.app.Application
import br.com.iuji.features.home.di.HomeModule
import br.com.iuji.features.home.di.TestModule
import kotlinx.serialization.ExperimentalSerializationApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

@ExperimentalSerializationApi
internal class TestApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TestApplication)
            HomeModule().load()
            TestModule().load()
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        HomeModule().load()
        TestModule().unload()
    }
}