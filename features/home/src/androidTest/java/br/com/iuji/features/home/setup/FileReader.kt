package br.com.iuji.features.home.setup

import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.serialization.ExperimentalSerializationApi
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader

object FileReader {
    @ExperimentalSerializationApi
    fun readStringFromFile(fileName: String): String {
        try {
            val inputStream = (InstrumentationRegistry.getInstrumentation().targetContext
                .applicationContext as TestApplication).assets.open(fileName)
            val builder = StringBuilder()
            val reader = InputStreamReader(inputStream, "UTF-8")
            reader.readLines().forEach {
                builder.append(it)
            }
            return builder.toString()
        } catch (e: IOException) {
            throw FileNotFoundException("file path: $fileName was not found")
        }
    }
}