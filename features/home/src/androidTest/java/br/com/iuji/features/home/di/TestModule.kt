package br.com.iuji.features.home.di

import br.com.iuji.features.home.presentation.HomeActivityRobot
import br.com.iuji.features.home.setup.StubRetrofitBuilder
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.module

internal class TestModule {
    private val testModule = module {
        factory(override = true) { HomeActivityRobot() }
        single(override = true) { StubRetrofitBuilder().buildRetrofit() }
    }

    fun load() = loadKoinModules(
        listOf(
            testModule
        )
    )

    fun unload() = unloadKoinModules(
        listOf(
            testModule
        )
    )
}