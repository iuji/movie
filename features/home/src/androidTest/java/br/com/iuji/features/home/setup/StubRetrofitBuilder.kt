package br.com.iuji.features.home.setup

import br.com.iuji.features.home.data.datasource.remote.service.MovieApi
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

private const  val BASE_URL = "http://localhost:8080/"
internal class StubRetrofitBuilder {

    private fun getLoggingInterceptor(): OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        return httpClient
    }

    @Suppress("EXPERIMENTAL_API_USAGE")
    fun buildRetrofit(): MovieApi {
        val contentType = "application/json".toMediaType()
        val json = Json { ignoreUnknownKeys = true }
        val converterFactory = json.asConverterFactory(contentType)
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(converterFactory)
            .client(getLoggingInterceptor().build())
            .build()
            .create(MovieApi::class.java)
    }
}