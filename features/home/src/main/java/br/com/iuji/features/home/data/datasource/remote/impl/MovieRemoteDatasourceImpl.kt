package br.com.iuji.features.home.data.datasource.remote.impl

import br.com.iuji.features.home.data.datasource.remote.MovieRemoteDatasource
import br.com.iuji.features.home.data.datasource.remote.model.MoviesResultResponse
import br.com.iuji.features.home.data.datasource.remote.service.MovieApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

internal class MovieRemoteDatasourceImpl(private val service: MovieApi) : MovieRemoteDatasource {
    override fun getMovies(): Flow<MoviesResultResponse> {
        return flow { emit(service.getMovies()) }
    }
}