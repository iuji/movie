package br.com.iuji.features.home.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import br.com.iuji.features.home.databinding.CardItemMovieBinding
import br.com.iuji.features.home.domain.model.MovieData

internal class MoviesAdapter(
    private val onItemClick: (movie: MovieData) -> Unit
) : ListAdapter<MovieData, MovieViewHolder>(MovieListDiff()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {

        val binding = CardItemMovieBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindItem(getItem(position), onItemClick)
    }
}

private class MovieListDiff : DiffUtil.ItemCallback<MovieData>() {
    override fun areItemsTheSame(oldItem: MovieData, newItem: MovieData): Boolean {
        return oldItem.id == newItem.id && oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: MovieData, newItem: MovieData): Boolean =
        newItem == oldItem
}