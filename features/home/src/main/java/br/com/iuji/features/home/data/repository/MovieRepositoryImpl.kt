package br.com.iuji.features.home.data.repository

import br.com.iuji.features.home.data.datasource.remote.MovieRemoteDatasource
import br.com.iuji.features.home.data.mapper.RemoteMovieMapper
import br.com.iuji.features.home.domain.model.MovieData
import br.com.iuji.features.home.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

internal class MovieRepositoryImpl(
    private val remoteDatasource: MovieRemoteDatasource,
    private val remoteMapper: RemoteMovieMapper,
) : MovieRepository {

    override fun getMovies(): Flow<List<MovieData>> {
        return remoteDatasource.getMovies().map(remoteMapper::map)
    }
}