package br.com.iuji.features.home.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.iuji.features.home.domain.model.MovieData
import br.com.iuji.features.home.domain.usecase.GetMoviesUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

internal class HomeViewModel(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
) : ViewModel() {

    private val _homeState = MutableLiveData<HomeState>()
    val homeState: LiveData<HomeState> get() = _homeState

    private val _homeAction = MutableLiveData<HomeAction>()
    val homeAction: LiveData<HomeAction> get() = _homeAction

    fun loadMovies() {
        viewModelScope.launch {
            getMoviesUseCase()
                .flowOn(dispatcher)
                .onStart { showLoading() }
                .catch { handleError() }
                .onCompletion { hideLoading() }
                .collect { handleSuccess(it) }
        }
    }

    private fun handleSuccess(movies: List<MovieData>) {
        _homeState.value = HomeState.SuccessResult(movies = movies)
    }

    private fun handleError() {
        _homeState.value = HomeState.Error
    }

    private fun showLoading() {
        _homeState.value = HomeState.Loading(isLoading = true)
    }

    private fun hideLoading() {
        _homeState.value = HomeState.Loading(isLoading = false)
    }

    fun onMovieClick(movie: MovieData) {
        _homeAction.value = HomeAction.OpenMovie(movie)
    }
}