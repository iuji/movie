package br.com.iuji.features.home.data.mapper

import br.com.iuji.core.mapper.Mapper
import br.com.iuji.features.home.data.datasource.remote.model.MovieResponse
import br.com.iuji.features.home.data.datasource.remote.model.MoviesResultResponse
import br.com.iuji.features.home.data.datasource.remote.model.toMovieData
import br.com.iuji.features.home.domain.model.MovieData

internal class RemoteMovieMapper : Mapper<MoviesResultResponse, List<MovieData>> {
    override fun map(source: MoviesResultResponse): List<MovieData> {
        return source.results.mapToListOfMovieData()
    }

    private fun List<MovieResponse>.mapToListOfMovieData(): List<MovieData> = map {
        it.toMovieData()
    }
}