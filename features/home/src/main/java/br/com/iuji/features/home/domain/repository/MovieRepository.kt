package br.com.iuji.features.home.domain.repository

import br.com.iuji.features.home.domain.model.MovieData
import kotlinx.coroutines.flow.Flow

internal interface MovieRepository {
    fun getMovies() : Flow<List<MovieData>>
}