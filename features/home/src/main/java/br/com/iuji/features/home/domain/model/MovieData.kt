package br.com.iuji.features.home.domain.model

internal data class MovieData(
    val id: Int,
    val title: String,
    val overview: String,
    val originalLanguage: String,
    val originalTitle: String,
    val voteAverage: Double,
    val posterPath: String,
    val backdropPath: String,
    val releaseDate: String
)
