package br.com.iuji.features.home.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.GridLayoutManager
import br.com.iuji.features.home.databinding.ActivityHomeBinding
import br.com.iuji.features.home.domain.model.MovieData
import br.com.iuji.features.home.presentation.adapter.MoviesAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private val viewModel: HomeViewModel by viewModel()
    private val adapter = MoviesAdapter(::onMovieClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        observeState()
        observeAction()
        setupSwipeToRefresh()
        setupRecyclerView()
        viewModel.loadMovies()
    }

    private fun observeAction() {
        viewModel.homeAction.nonNullObserve(this) { state ->
            when (state) {
                is HomeAction.OpenMovie -> openMovie(state.movie)
            }
        }
    }

    private fun openMovie(movie: MovieData) {
        Toast.makeText(this, "Clicou no ${movie.title}", Toast.LENGTH_SHORT).show()
    }

    private fun observeState() {
        viewModel.homeState.nonNullObserve(this) { state ->
            when (state) {
                is HomeState.Loading -> onLoadingState(state.isLoading)
                is HomeState.SuccessResult -> onShowMovies(state.movies)
                is HomeState.Error -> onErrorState()
            }
        }
    }

    private fun onLoadingState(isLoading: Boolean) {
        binding.swipeRefreshLayout.isRefreshing = isLoading
    }

    private fun onShowMovies(movies: List<MovieData>) = with(binding) {
        errorView.root.isVisible = false
        swipeRefreshLayout.isVisible = true
        adapter.submitList(movies)
    }

    private fun onErrorState() = with(binding) {
        swipeRefreshLayout.isVisible = false
        errorView.root.isVisible = true
        errorView.homeGenericErrorButton.setOnClickListener {
            viewModel.loadMovies()
        }
    }

    private fun setupSwipeToRefresh() = with(binding) {
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.loadMovies()
        }
    }

    private fun setupRecyclerView() {
        with(binding) {
            moviesRecycler.adapter = adapter
            moviesRecycler.layoutManager = GridLayoutManager(this@HomeActivity, 2)
        }
    }

    private fun onMovieClick(movie: MovieData) {
        viewModel.onMovieClick(movie)
    }

    private fun <T> LiveData<T>.nonNullObserve(owner: LifecycleOwner, observer: (data: T) -> Unit) {
        observe(owner, {
            it?.let(observer)
        })
    }

}