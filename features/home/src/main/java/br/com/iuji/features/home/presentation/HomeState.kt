package br.com.iuji.features.home.presentation

import br.com.iuji.features.home.domain.model.MovieData

internal sealed class HomeState{
    data class Loading(val isLoading: Boolean) : HomeState()
    data class SuccessResult(val movies: List<MovieData>) : HomeState()
    object Error : HomeState()
}