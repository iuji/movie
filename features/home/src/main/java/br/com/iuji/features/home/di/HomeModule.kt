package br.com.iuji.features.home.di

import br.com.iuji.features.home.BuildConfig
import br.com.iuji.features.home.data.datasource.remote.MovieRemoteDatasource
import br.com.iuji.features.home.data.datasource.remote.impl.MovieRemoteDatasourceImpl
import br.com.iuji.features.home.data.datasource.remote.service.MovieApi
import br.com.iuji.features.home.data.mapper.RemoteMovieMapper
import br.com.iuji.features.home.data.repository.MovieRepositoryImpl
import br.com.iuji.features.home.domain.repository.MovieRepository
import br.com.iuji.features.home.domain.usecase.GetMoviesUseCase
import br.com.iuji.features.home.navigation.HomeNavigationImpl
import br.com.iuji.features.home.presentation.HomeViewModel
import br.com.iuji.navigation.features.home.HomeNavigation
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

@ExperimentalSerializationApi
class HomeModule {


    private fun getLoggingInterceptor(): OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        return httpClient
    }

    private val remoteDataModule = module {
        val contentType = "application/json".toMediaType()
        val json = Json { ignoreUnknownKeys = true }
        val converterFactory = json.asConverterFactory(contentType)
        single {
            Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL_API)
                .client(getLoggingInterceptor().build())
                .addConverterFactory(converterFactory)
                .build()
                .create(MovieApi::class.java)
        }
        factory<MovieRemoteDatasource> {
            MovieRemoteDatasourceImpl(service = get())
        }
    }

    private val repositoryModule = module {
        factory<MovieRepository> {
            MovieRepositoryImpl(
                remoteDatasource = get(),
                remoteMapper = RemoteMovieMapper()
            )
        }
    }

    private val navigationModule = module {
        factory<HomeNavigation> {
            HomeNavigationImpl()
        }
    }

    private val presentationModule = module {
        viewModel{
            HomeViewModel(
                getMoviesUseCase = GetMoviesUseCase(
                    repository = get()
                )
            )
        }
    }


    fun load() {
        loadKoinModules(
            listOf(
                remoteDataModule,
                navigationModule,
                presentationModule,
                repositoryModule
            )
        )
    }
}