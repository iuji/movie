package br.com.iuji.features.home.navigation

import android.content.Context
import android.content.Intent
import br.com.iuji.features.home.presentation.HomeActivity
import br.com.iuji.navigation.features.home.HomeNavigation

class HomeNavigationImpl : HomeNavigation {
    override fun startActivity(context: Context) {
        val intent = Intent(context, HomeActivity::class.java)
        context.startActivity(intent)
    }
}