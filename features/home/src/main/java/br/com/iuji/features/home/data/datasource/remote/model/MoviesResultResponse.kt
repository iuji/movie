package br.com.iuji.features.home.data.datasource.remote.model

import br.com.iuji.features.home.domain.model.MovieData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoviesResultResponse (
    @SerialName("results")
    val results: List<MovieResponse>
)

@Serializable
internal data class MovieResponse(

    @SerialName("id")
    val id: Int,

    @SerialName("title")
    val title: String,

    @SerialName("overview")
    val overview: String,

    @SerialName("original_language")
    val originalLanguage: String,

    @SerialName("original_title")
    val originalTitle: String,

    @SerialName("vote_average")
    val voteAverage: Double,

    @SerialName("poster_path")
    val posterPath: String,

    @SerialName("backdrop_path")
    val backdropPath: String,

    @SerialName("release_date")
    val releaseDate: String
)

internal fun MovieResponse.toMovieData() : MovieData {
    return MovieData (
        id = this.id,
        title = this.title,
        overview = this.overview,
        originalLanguage = this.originalLanguage,
        originalTitle = this.originalTitle,
        voteAverage = this.voteAverage,
        posterPath = this.posterPath,
        backdropPath = this.backdropPath,
        releaseDate = this.releaseDate,
    )
}