package br.com.iuji.features.home.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import br.com.iuji.features.home.BuildConfig
import br.com.iuji.features.home.R
import br.com.iuji.features.home.databinding.CardItemMovieBinding
import br.com.iuji.features.home.domain.model.MovieData
import com.squareup.picasso.Picasso

internal class MovieViewHolder(private val binding: CardItemMovieBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindItem(
        movie: MovieData,
        onItemClick: (movie: MovieData) -> Unit
    ) = with(binding) {
        val urlImage = BuildConfig.BASE_URL_IMAGE + movie.posterPath
        Picasso.get()
            .load(urlImage)
            .placeholder(R.drawable.placeholder)
            .into(movieImage)
        movieTitle.text = movie.title
        movieCard.setOnClickListener {
            onItemClick.invoke(movie)
        }
    }
}