package br.com.iuji.features.home.data.datasource.remote

import br.com.iuji.features.home.data.datasource.remote.model.MoviesResultResponse
import kotlinx.coroutines.flow.Flow

internal interface MovieRemoteDatasource {
    fun getMovies(): Flow<MoviesResultResponse>
}