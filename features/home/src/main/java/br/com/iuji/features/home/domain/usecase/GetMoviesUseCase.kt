package br.com.iuji.features.home.domain.usecase

import br.com.iuji.features.home.domain.model.MovieData
import br.com.iuji.features.home.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow

internal class GetMoviesUseCase(
    private val repository: MovieRepository
) {
    operator fun invoke(): Flow<List<MovieData>> {
        return repository.getMovies()
    }
}