package br.com.iuji.features.home.presentation

import br.com.iuji.features.home.domain.model.MovieData

internal sealed class HomeAction {
    data class OpenMovie(val movie: MovieData) : HomeAction()
}
