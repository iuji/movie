package br.com.iuji.features.home.data.datasource.remote.service

import br.com.iuji.features.home.BuildConfig
import br.com.iuji.features.home.data.datasource.remote.model.MoviesResultResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface MovieApi {
    @GET("{search_type}")
    suspend fun getMovies(
        @Path("search_type") searchType: String = BuildConfig.SEARCH_TYPE,
        @Query("api_key") apiKey: String = BuildConfig.API_KEY,
        @Query("language") language: String = BuildConfig.LANGUAGE
    ): MoviesResultResponse
}