package br.com.iuji.features.home.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.iuji.features.home.domain.usecase.GetMoviesUseCase
import br.com.iuji.features.home.stub.MovieStub.defaultThrowable
import br.com.iuji.features.home.stub.MovieStub.errorState
import br.com.iuji.features.home.stub.MovieStub.hideLoadingState
import br.com.iuji.features.home.stub.MovieStub.movieDate
import br.com.iuji.features.home.stub.MovieStub.moviesData
import br.com.iuji.features.home.stub.MovieStub.openMovieAction
import br.com.iuji.features.home.stub.MovieStub.showLoadingState
import br.com.iuji.features.home.stub.MovieStub.successState
import br.com.iuji.features.home.utils.CoroutineTestRule
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class HomeViewModelTest {
    @get:Rule
    val instantTaskRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private val useCase = mockk<GetMoviesUseCase>()
    private lateinit var viewModel: HomeViewModel
    private val actionObserver: Observer<HomeAction> = mockk(relaxed = true)
    private val stateObserver: Observer<HomeState> = mockk(relaxed = true)

    @Before
    fun setUp() {
        viewModel = HomeViewModel(useCase, coroutineTestRule.testDispatcher)
        viewModel.homeState.observeForever(stateObserver)
        viewModel.homeAction.observeForever(actionObserver)
    }

    @Test
    fun `loadMovies should set success state when receive movieData list`() = runBlockingTest {
        // Given
        every { useCase() } returns flowOf(moviesData)

        // When
        viewModel.loadMovies()

        // Then
        verify {
            stateObserver.onChanged(showLoadingState)
            stateObserver.onChanged(successState)
            stateObserver.onChanged(hideLoadingState)
        }
    }

    @Test
    fun `loadMovies should set error state when receive a throwable`() = runBlockingTest {
        // Given
        every { useCase() } returns flow { throw defaultThrowable }

        // When
        viewModel.loadMovies()

        // Then
        verify {
            stateObserver.onChanged(showLoadingState)
            stateObserver.onChanged(errorState)
            stateObserver.onChanged(hideLoadingState)
        }
    }

    @Test
    fun `onMovieClick should set open movie action when called`() = runBlockingTest {
        // Given
        val expectedMovie = movieDate

        // When
        viewModel.onMovieClick(expectedMovie)

        // Then
        verify {
            actionObserver.onChanged(openMovieAction)
        }
    }

}