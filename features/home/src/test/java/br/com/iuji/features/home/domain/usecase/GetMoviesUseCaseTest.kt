package br.com.iuji.features.home.domain.usecase

import app.cash.turbine.test
import br.com.iuji.features.home.domain.repository.MovieRepository
import br.com.iuji.features.home.stub.MovieStub.moviesData
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.time.ExperimentalTime

@ExperimentalTime
internal class GetMoviesUseCaseTest {
    private val repository = mockk<MovieRepository>()
    private val useCase = GetMoviesUseCase(repository)

    @Test
    fun `GetMoviesUseCase should return a valid list of MovieData`() = runBlocking {
        // Given
        val expected = moviesData
        every { repository.getMovies() } returns flowOf(expected)

        // When
        val result = useCase()

        // Then
        result.test {
            verify { repository.getMovies() }
            assertEquals(expected, expectItem())
            expectComplete()
        }
    }
}