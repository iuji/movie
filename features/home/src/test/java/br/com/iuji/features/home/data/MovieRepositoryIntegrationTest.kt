package br.com.iuji.features.home.data

import app.cash.turbine.test
import br.com.iuji.features.home.data.datasource.remote.impl.MovieRemoteDatasourceImpl
import br.com.iuji.features.home.data.datasource.remote.service.MovieApi
import br.com.iuji.features.home.data.mapper.RemoteMovieMapper
import br.com.iuji.features.home.data.repository.MovieRepositoryImpl
import br.com.iuji.features.home.domain.repository.MovieRepository
import br.com.iuji.features.home.stub.MovieStub.moviesData
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf
import org.junit.After
import org.junit.Test
import retrofit2.Retrofit
import java.io.FileNotFoundException
import java.net.URL
import kotlin.test.assertEquals
import kotlin.time.ExperimentalTime

@ExperimentalSerializationApi
@ExperimentalTime
internal class MovieRepositoryIntegrationTest {
    private val mockWebServer: MockWebServer by lazy {
        MockWebServer()
    }
    private val retrofit = createRetrofit(mockWebServer)
    private val service: MovieApi = retrofit.create(MovieApi::class.java)
    private val repository = createRepository(service)

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `getMovies should return a MoviesResultResponse`() = runBlocking {
        // Given
        val expectedPath = "/popular?api_key=79e6996a3fb5868292133e297edb0308&language=pt-BR"
        val expectedResult = moviesData
        mockWebServer.dispatcher = setupDispatcher(successResponse())


        // When
        val result = repository.getMovies()

        // Then
        result.test {
            assertEquals(expectedResult, expectItem())
            assertEquals(expectedPath, mockWebServer.takeRequest().path)
            expectComplete()
        }

    }

    @Test
    fun `getMovies Should throw a Throwable error`() = runBlocking {
        // Given
        val expectedResult = IsInstanceOf(Throwable::class.java)
        mockWebServer.dispatcher = setupDispatcher(errorResponse())


        // When
        val result = repository.getMovies()
        result.test {
            assertThat(expectError(), expectedResult)
            expectNoEvents()
        }

    }

    private fun setupDispatcher(mockResponse: MockResponse): Dispatcher {
        return object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return mockResponse
            }
        }
    }

    private fun successResponse() = MockResponse()
        .setBody(readFile("home/home_movies_success_response.json"))
        .setResponseCode(200)

    private fun errorResponse() = MockResponse()
        .setBody(readFile("home/api_generic_error_response.json"))
        .setResponseCode(401)

    private fun createRetrofit(
        server: MockWebServer
    ): Retrofit {
        val contentType = "application/json".toMediaType()
        val json = Json { ignoreUnknownKeys = true }
        val converterFactory = json.asConverterFactory(contentType)
        return Retrofit.Builder()
            .baseUrl(server.url(""))
            .addConverterFactory(converterFactory)
            .build()
    }

    private fun readFile(path: String): String {
        val content: URL? = ClassLoader.getSystemResource(path)
        return content?.readText() ?: throw FileNotFoundException("file path: $path was not found")
    }

    private fun createRepository(service: MovieApi): MovieRepository {
        val remoteDataSource = MovieRemoteDatasourceImpl(service)
        val remoteMapper = RemoteMovieMapper()

        return MovieRepositoryImpl(remoteDataSource, remoteMapper)
    }
}