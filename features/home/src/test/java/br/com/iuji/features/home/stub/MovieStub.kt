package br.com.iuji.features.home.stub

import br.com.iuji.features.home.data.datasource.remote.model.MovieResponse
import br.com.iuji.features.home.data.datasource.remote.model.MoviesResultResponse
import br.com.iuji.features.home.domain.model.MovieData
import br.com.iuji.features.home.presentation.HomeAction
import br.com.iuji.features.home.presentation.HomeState

internal object MovieStub {

    internal val movieDate = MovieData(
        id = 476669,
        title = "King's Man: A Origem",
        overview = "Quando uma série dos piores tiranos e gênios do crime da história se juntam para criar uma guerra que aniquilará milhões, um homem e seu pupilo precisam correr contra o tempo para pará-los.",
        originalLanguage = "en",
        originalTitle = "The King's Man",
        voteAverage = 7.2,
        posterPath = "/pVL9AyKKLfUwrYD6jhdsI15gBQ7.jpg",
        backdropPath = "/6qkeXdIEwqOuOWuxsomwnin2RdD.jpg",
        releaseDate = "2021-12-22",
    )

    internal val moviesData = listOf(
        MovieData(
            id = 634649,
            title = "Homem-Aranha: Sem Volta Para Casa",
            overview = "Peter Parker é desmascarado e não consegue mais separar sua vida normal dos grandes riscos de ser um super-herói. Quando ele pede ajuda ao Doutor Estranho, os riscos se tornam ainda mais perigosos, e o forçam a descobrir o que realmente significa ser o Homem-Aranha.",
            originalLanguage = "en",
            originalTitle = "Spider-Man: No Way Home",
            voteAverage = 8.4,
            posterPath = "/fVzXp3NwovUlLe7fvoRynCmBPNc.jpg",
            backdropPath = "/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg",
            releaseDate = "2021-12-15",
        ),
        MovieData(
            id = 476669,
            title = "King's Man: A Origem",
            overview = "Quando uma série dos piores tiranos e gênios do crime da história se juntam para criar uma guerra que aniquilará milhões, um homem e seu pupilo precisam correr contra o tempo para pará-los.",
            originalLanguage = "en",
            originalTitle = "The King's Man",
            voteAverage = 7.2,
            posterPath = "/pVL9AyKKLfUwrYD6jhdsI15gBQ7.jpg",
            backdropPath = "/6qkeXdIEwqOuOWuxsomwnin2RdD.jpg",
            releaseDate = "2021-12-22",
        )
    )

    internal val moviesResponse = MoviesResultResponse(
        results = listOf(
            MovieResponse(
                id = 634649,
                title = "Homem-Aranha: Sem Volta Para Casa",
                overview = "Peter Parker é desmascarado e não consegue mais separar sua vida normal dos grandes riscos de ser um super-herói. Quando ele pede ajuda ao Doutor Estranho, os riscos se tornam ainda mais perigosos, e o forçam a descobrir o que realmente significa ser o Homem-Aranha.",
                originalLanguage = "en",
                originalTitle = "Spider-Man: No Way Home",
                voteAverage = 8.4,
                posterPath = "/fVzXp3NwovUlLe7fvoRynCmBPNc.jpg",
                backdropPath = "/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg",
                releaseDate = "2021-12-15",
            ),
            MovieResponse(
                id = 476669,
                title = "King's Man: A Origem",
                overview = "Quando uma série dos piores tiranos e gênios do crime da história se juntam para criar uma guerra que aniquilará milhões, um homem e seu pupilo precisam correr contra o tempo para pará-los.",
                originalLanguage = "en",
                originalTitle = "The King's Man",
                voteAverage = 7.2,
                posterPath = "/pVL9AyKKLfUwrYD6jhdsI15gBQ7.jpg",
                backdropPath = "/6qkeXdIEwqOuOWuxsomwnin2RdD.jpg",
                releaseDate = "2021-12-22",
            )
        )
    )

    internal val showLoadingState = HomeState.Loading(isLoading = true)
    internal val hideLoadingState = HomeState.Loading(isLoading = false)
    internal val successState = HomeState.SuccessResult(movies = moviesData)
    internal val errorState = HomeState.Error
    internal val openMovieAction = HomeAction.OpenMovie(movieDate)

    internal val defaultThrowable = Throwable()
}